﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTesteTecnico.Models
{
    public class Empresa
    {
       public int IdEmpresa { get; set; }
        public string Uf { get; set; }
        public string NomeFantasia { get; set; }
        public string Cnpj { get; set; }
    }
}
