﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTesteTecnico.Models
{
    public class EmpresaFornecedor
    {
        [ForeignKey("IdEmpresa")]
        public int IdEmpresa { get; set; }

        [ForeignKey("IdFornecedor")]
        public int IdFornecedor { get; set; }

        public virtual Empresa _clVirtualEmpresa { get; set; }
        public virtual Fornecedor _clVirtualFornecedor { get; set; }

    }
}
