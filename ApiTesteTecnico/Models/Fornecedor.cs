﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTesteTecnico.Models
{
    public class Fornecedor
    {
        public int IdFornecedor { get; set; }
        public string Empresa { get; set; }
        public string Nome { get; set; }
        public string CpfCnpj { get; set; }
        public String DataCadastro { get; set; }
        public string Telefone { get; set; }
        public string DataNascimento{ get; set; }
        public string Rg { get; set; }

    }
}