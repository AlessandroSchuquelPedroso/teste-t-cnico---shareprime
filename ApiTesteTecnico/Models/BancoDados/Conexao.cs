﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTesteTecnico.Models
{
    public class Conexao
    {
        private SqlConnection _strConnection;

        public Conexao(string conexao)
        {
            this._strConnection = new SqlConnection(conexao);
        }

        public SqlConnection Connection
        {
            get => _strConnection;
            set => _strConnection = value;
        }

        public void Conectar()
        {
            try
            {
                _strConnection.Open();
            }
            catch (Exception)
            {

                throw;
            }
           
        }

        public void Desconectar()
        {
            try
            {
                _strConnection.Close();
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
