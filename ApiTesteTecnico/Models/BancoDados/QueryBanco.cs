﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ApiTesteTecnico.Models.BancoDados
{
    public class QueryBanco
    {
        private const string STR_CONNECTION = @"Server=localhost\SQLEXPRESS;Database=testeTecnico;Trusted_Connection=True";
        
        // cadastra a empresa no banco
        public void CadastraEmpresa(Empresa empresa)
        {
            Conexao _connection = new Conexao(STR_CONNECTION);
            using (_connection.Connection)
            {
                //abre conexao
                _connection.Conectar();

                string queryInsert = "insert into dbo.empresa (uf,nome_fantasia,cnpj) values (@uf,@empresa,@cnpj)";

                using (SqlCommand comando = new SqlCommand(queryInsert, _connection.Connection))
                {
                    comando.Parameters.AddWithValue("@uf", empresa.Uf);
                    comando.Parameters.AddWithValue("@empresa", empresa.NomeFantasia);
                    comando.Parameters.AddWithValue("@cnpj", empresa.Cnpj);
                    comando.ExecuteNonQuery();
                }


                    //fecha conexao
                    _connection.Desconectar();
            }
        }

        //verifica se contem o mesmo cnpj na tabela empresa
        public Boolean VerificaCnpjEmpresa(Empresa empresa)
        {
            Conexao _connectionVerificaCnpj = new Conexao(STR_CONNECTION);

            string cnpj = String.Empty;
            using (_connectionVerificaCnpj.Connection)
            {
                //abre conexão
                _connectionVerificaCnpj.Conectar();

                string query = "select cnpj from dbo.empresa where cnpj = @cnpj";
                
                using (SqlCommand cmd = new SqlCommand(query, _connectionVerificaCnpj.Connection))
                {

                    cmd.Parameters.AddWithValue("@cnpj", empresa.Cnpj);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        cnpj = reader[0].ToString();

                    }
                    reader.Close();

                }

                    //fecha conexao
                  _connectionVerificaCnpj.Desconectar();
            }

            return  cnpj == empresa.Cnpj ? true : false;
        }

        //Empresas Cadastradas
        public List<Empresa> EmpresasCadastradas()
        {
            Conexao _connectionEmpresasCadastradas = new Conexao(STR_CONNECTION);
            Empresa empresa = null;
            List<Empresa> listEmpresa = new List<Empresa>();

            using (_connectionEmpresasCadastradas.Connection)
            {
                _connectionEmpresasCadastradas.Conectar();

                string query = "select * from empresa";

                using (SqlCommand cmd = new SqlCommand(query, _connectionEmpresasCadastradas.Connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        empresa = new Empresa
                        {
                            IdEmpresa = reader["idEmpresa"] == DBNull.Value ? 0 : (Int32)reader["idEmpresa"],
                            Uf = reader["uf"] == DBNull.Value ? string.Empty : (String)reader["uf"],
                            NomeFantasia = reader["nome_fantasia"] == DBNull.Value ? string.Empty: (String)reader["nome_fantasia"],
                            Cnpj = reader["cnpj"] == DBNull.Value ? string.Empty : (String)reader["cnpj"]

                        };

                        listEmpresa.Add(empresa);
                    }
                    reader.Close();
                    
                  
                }

                    _connectionEmpresasCadastradas.Desconectar();
            }

            return listEmpresa;
        }

        // cadastrar fornecedor
        public void CadastrarFornecedor(Fornecedor fornecedor)
        {
            //DateTime dataSistema = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            string dataSistema = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
            Conexao _connectionCadastrarFornecedor = new Conexao(STR_CONNECTION);

            using (_connectionCadastrarFornecedor.Connection)
            {
                //abre conexao
                _connectionCadastrarFornecedor.Conectar();

                string query = "insert into dbo.fornecedor(empresa,nome,cpf_cnpj,data_cadastro,telefone,data_nascimento,rg) values (@empresa,@nome,@cpf_cnpj,@data_cadastro,@telefone,@data_nascimento,@rg)";

                using (SqlCommand cmd = new SqlCommand(query, _connectionCadastrarFornecedor.Connection))
                {
                    cmd.Parameters.AddWithValue("@empresa",fornecedor.Empresa);
                    cmd.Parameters.AddWithValue("@nome",fornecedor.Nome);
                    cmd.Parameters.AddWithValue("@cpf_cnpj",fornecedor.CpfCnpj);
                    cmd.Parameters.AddWithValue("@data_cadastro", dataSistema);
                    cmd.Parameters.AddWithValue("@telefone",fornecedor.Telefone);
                    cmd.Parameters.AddWithValue("@data_nascimento",fornecedor.DataNascimento);
                    cmd.Parameters.AddWithValue("rg",fornecedor.Rg);
                    cmd.ExecuteNonQuery();

                }

                    // fecha conexao
                    _connectionCadastrarFornecedor.Desconectar();
            }
        }
        
        // verifica se já existe cnpjEcpf
        public Boolean VerificaCnpjECpf(Fornecedor fornecedor)
        {
            Conexao _connectionVerificaCnpjECpf = new Conexao(STR_CONNECTION);

            string cnpjCpf = String.Empty;
            using (_connectionVerificaCnpjECpf.Connection)
            {
                //abre conexão
                _connectionVerificaCnpjECpf.Conectar();

                string query = "select cpf_cnpj from dbo.fornecedor where cpf_cnpj = @cpf_cnpj";

                using (SqlCommand cmd = new SqlCommand(query, _connectionVerificaCnpjECpf.Connection))
                {

                    cmd.Parameters.AddWithValue("@cpf_cnpj", fornecedor.CpfCnpj);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        cnpjCpf = reader[0].ToString();

                    }
                    reader.Close();

                }

                //fecha conexao
                _connectionVerificaCnpjECpf.Desconectar();
            }

            return cnpjCpf == fornecedor.CpfCnpj ? true : false;
        }

        // Fornecedores cadastrados
        public List<Fornecedor> FornecedoresCadastrados()
        {
            Conexao _connectionFornecedoresCadastrados = new Conexao(STR_CONNECTION);
            Fornecedor fornecedores = null;
            List<Fornecedor> listFornecedor = new List<Fornecedor>();

            using (_connectionFornecedoresCadastrados.Connection)
            {
                _connectionFornecedoresCadastrados.Conectar();

                string query = "select * from fornecedor";

                using (SqlCommand cmd = new SqlCommand(query, _connectionFornecedoresCadastrados.Connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        fornecedores = new Fornecedor
                        {
                            IdFornecedor = reader["idFornecedor"] == DBNull.Value ? 0 : (Int32)reader["idFornecedor"],
                            Empresa = reader["empresa"] == DBNull.Value ? string.Empty : (String)reader["empresa"],
                            Nome = reader["nome"] == DBNull.Value ? string.Empty : (String)reader["nome"],
                            CpfCnpj = reader["cpf_cnpj"] == DBNull.Value ? string.Empty : (String)reader["cpf_cnpj"],
                            DataCadastro = reader["data_cadastro"] == DBNull.Value ? String.Empty: (String)reader["data_cadastro"].ToString(),
                            Telefone = reader["telefone"] == DBNull.Value ? string.Empty : (String)reader["telefone"],
                            DataNascimento = reader["data_nascimento"] == DBNull.Value ? string.Empty : (String)reader["data_nascimento"],
                            Rg = reader["rg"] == DBNull.Value ? string.Empty : (String)reader["rg"],

                        };

                        listFornecedor.Add(fornecedores);
                    }
                    reader.Close();
                }
                _connectionFornecedoresCadastrados.Desconectar();
            }

            return listFornecedor;
        }

        // busca fornecedor pelo id
        public Fornecedor BuscaFornecedor(int idFornecedor)
        {
            Conexao _connFornecedore = new Conexao(STR_CONNECTION);
            Fornecedor fornecedores = null;
            List<Fornecedor> listFornecedor = new List<Fornecedor>();

            using (_connFornecedore.Connection)
            {
                _connFornecedore.Conectar();

                string query = "select * from fornecedor where idFornecedor = @idFornecedor";

                using (SqlCommand cmd = new SqlCommand(query, _connFornecedore.Connection))
                {
                    cmd.Parameters.AddWithValue("@idFornecedor", idFornecedor);

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        fornecedores = new Fornecedor
                        {
                            IdFornecedor = reader["idFornecedor"] == DBNull.Value ? 0 : (Int32)reader["idFornecedor"],
                            Empresa = reader["empresa"] == DBNull.Value ? string.Empty : (String)reader["empresa"],
                            Nome = reader["nome"] == DBNull.Value ? string.Empty : (String)reader["nome"],
                            CpfCnpj = reader["cpf_cnpj"] == DBNull.Value ? string.Empty : (String)reader["cpf_cnpj"],
                            DataCadastro = reader["data_cadastro"] == DBNull.Value ? String.Empty : (String)reader["data_cadastro"].ToString(),
                            Telefone = reader["telefone"] == DBNull.Value ? string.Empty : (String)reader["telefone"],
                            DataNascimento = reader["data_nascimento"] == DBNull.Value ? string.Empty : (String)reader["data_nascimento"],
                            Rg = reader["rg"] == DBNull.Value ? string.Empty : (String)reader["rg"],

                        };


                    }
                    reader.Close();
                }
                _connFornecedore.Desconectar();
            }

            return fornecedores;
        }

        //busca empresa pelo id
        public Empresa BuscaEmpresa(int idEmpresa)
        {
            Conexao _connEmpresa = new Conexao(STR_CONNECTION);
            Empresa empresa = null;
           

            using (_connEmpresa.Connection)
            {
                _connEmpresa.Conectar();

                string query = "select * from empresa where idEmpresa = @idEmpresa";

                using (SqlCommand cmd = new SqlCommand(query, _connEmpresa.Connection))
                {
                    cmd.Parameters.AddWithValue("@idEmpresa", idEmpresa);
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        empresa = new Empresa
                        {
                            IdEmpresa = reader["idEmpresa"] == DBNull.Value ? 0 : (Int32)reader["idEmpresa"],
                            Uf = reader["uf"] == DBNull.Value ? string.Empty : (String)reader["uf"],
                            NomeFantasia = reader["nome_fantasia"] == DBNull.Value ? string.Empty : (String)reader["nome_fantasia"],
                            Cnpj = reader["cnpj"] == DBNull.Value ? string.Empty : (String)reader["cnpj"]

                        };
                    }
                    reader.Close();
                }

                _connEmpresa.Desconectar();
            }
            return empresa;
        }

        // faz o insert do fornecedor há empresa
        public void InsertRelacaoEmpresaFornecedor(int idFornecedor, int idEmpresa)
        {
            Conexao _connectionInsertRelacao = new Conexao(STR_CONNECTION);

            using (_connectionInsertRelacao.Connection)
            {
                // abre conexão
                _connectionInsertRelacao.Conectar();

                string query = "insert into empresaFornecedor (idFornecedor,idEmpresa) values (@idFornecedor, @idEmpresa)";

                using (SqlCommand cmd = new SqlCommand(query, _connectionInsertRelacao.Connection))
                {
                    cmd.Parameters.AddWithValue("@idFornecedor", idFornecedor);
                    cmd.Parameters.AddWithValue("@idEmpresa", idEmpresa);
                    cmd.ExecuteReader();
                }
                    // fecha conexão
                    _connectionInsertRelacao.Desconectar();
            }
        }

        //faz a busca por filtro nome, cnpj/cpf ou hora do cadastramento
        public List<Object> FiltraFornecedores(string filtro)
        {
            Conexao _connectionFiltra = new Conexao(STR_CONNECTION);

            List<Object> listFornecedores = new List<Object>();

            using (_connectionFiltra.Connection)
            {
                //abre conexao
                _connectionFiltra.Conectar();

                string query = "select empForn.[idEmpresa],empForn.[idFornecedor] ,emp.[nome_fantasia],emp.[uf],emp.[cnpj],forn.[nome],forn.[empresa],forn.[cpf_cnpj],forn.[rg],forn.[data_nascimento],forn.[telefone],forn.[data_cadastro] from empresaFornecedor empForn"
                    + " LEFT JOIN empresa emp on empForn.idEmpresa = emp.idEmpresa"
                    + " LEFT JOIN fornecedor forn on empForn.idFornecedor = forn.idFornecedor"
                    + $" where forn.nome like '{filtro}%' or forn.cpf_cnpj = '{filtro}' or forn.data_cadastro like '{filtro}%'";

                using (SqlCommand cmd = new SqlCommand(query, _connectionFiltra.Connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        EmpresaFornecedor empresaFornecedor = new EmpresaFornecedor();
                        empresaFornecedor.IdEmpresa = reader["idEmpresa"] == DBNull.Value ? 0 : (Int32)reader["idEmpresa"];
                        empresaFornecedor.IdFornecedor = reader["idFornecedor"] == DBNull.Value ? 0 : (Int32)reader["idFornecedor"];

                        Empresa empresa = new Empresa();
                        empresa.IdEmpresa = reader["idEmpresa"] == DBNull.Value ? 0 : (Int32)reader["idEmpresa"];
                        empresa.NomeFantasia = reader["nome_fantasia"] == DBNull.Value ? string.Empty : (String)reader["nome_fantasia"];
                        empresa.Uf = reader["uf"] == DBNull.Value ? string.Empty : (String)reader["uf"];
                        empresa.Cnpj = reader["cnpj"] == DBNull.Value ? string.Empty : (String)reader["cnpj"];

                        empresaFornecedor._clVirtualEmpresa = empresa;

                        Fornecedor fornecedor = new Fornecedor();
                        fornecedor.IdFornecedor = reader["idFornecedor"] == DBNull.Value ? 0 : (Int32)reader["idFornecedor"];
                        fornecedor.Nome = reader["nome"] == DBNull.Value ? string.Empty : (String)reader["nome"];
                        fornecedor.Empresa = reader["empresa"] == DBNull.Value ? string.Empty : (String)reader["empresa"];
                        fornecedor.CpfCnpj = reader["cpf_cnpj"] == DBNull.Value ? string.Empty : (String)reader["cpf_cnpj"];
                        fornecedor.Rg = reader["rg"] == DBNull.Value ? string.Empty : (String)reader["rg"];
                        fornecedor.DataNascimento = reader["data_nascimento"] == DBNull.Value ? string.Empty : (String)reader["data_nascimento"];
                        fornecedor.Telefone = reader["telefone"] == DBNull.Value ? string.Empty : (String)reader["telefone"];
                        fornecedor.DataCadastro = reader["data_cadastro"] == DBNull.Value ? string.Empty : (String)reader["data_cadastro"].ToString();

                        empresaFornecedor._clVirtualFornecedor = fornecedor;

                        listFornecedores.Add(empresaFornecedor);
                        
                    }
                    reader.Close();
                }

                    //fecha conexao
                    _connectionFiltra.Desconectar();
            }

            return listFornecedores;
        }

    }
}
