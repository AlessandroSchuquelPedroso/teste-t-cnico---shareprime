﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiTesteTecnico.Models;
using ApiTesteTecnico.Models.BancoDados;
using Microsoft.AspNetCore.Mvc;

namespace ApiTesteTecnico.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // acessar querys do banco
        QueryBanco query = new QueryBanco();

        // POST api/values/cadastra/empresa
        [HttpPost("cadastrar/empresa")]
        public ActionResult PostCadastrarEmpresa(Empresa empresa)
        {
            try
            {
                Boolean verificaCnpj = false;
                verificaCnpj = query.VerificaCnpjEmpresa(empresa);
                if (verificaCnpj == true)
                {
                    return StatusCode(403, "CNPJ já cadastrado!");
                }

                query.CadastraEmpresa(empresa);

                return StatusCode(201, empresa);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }

        }

        // POST api/values/cadastra/fornecedor
        [HttpPost("cadastrar/fornecedor")]
        public ActionResult PostCadastrarFornecedor(Fornecedor fornecedor)
        {
            try
            {
                Boolean verificaCnpjEcpf = false;
                verificaCnpjEcpf = query.VerificaCnpjECpf(fornecedor);

                if (verificaCnpjEcpf == true)
                {
                    return StatusCode(403, "CPF OU CNPJ já cadastrado!");
                }

                query.CadastrarFornecedor(fornecedor);
                return StatusCode(201, fornecedor);
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

     
        }

        // POST api/values/cadastrar/relacao
        [HttpPost("cadastrar/relacao")]
        public ActionResult PostRelacao([FromHeader]int idFornecedor, [FromHeader]int idEmpresa)
        {
            try
            {
                Fornecedor fornecedor = new Fornecedor();
                Empresa empresa = new Empresa();
                fornecedor = query.BuscaFornecedor(idFornecedor);
                empresa = query.BuscaEmpresa(idEmpresa);

                if (fornecedor.CpfCnpj.Length == 11 && empresa.Uf == "pr" && Convert.ToDateTime(fornecedor.DataNascimento.ToString()).AddYears(18) > DateTime.Now)
                {
                    return StatusCode(403, "Empresa do Paraná não permite pessoa fisica menor de idade realizar o cadastro!");
                }

                query.InsertRelacaoEmpresaFornecedor(idFornecedor, idEmpresa);

                return StatusCode(201, "Relação cadastrada");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }


        }

        // GET api/values/empresas
        [HttpGet("empresas")]
        public ActionResult<List<Empresa>> Get()
        {

            try
            {
                List<Empresa> listEmpresa = new List<Empresa>();
                listEmpresa = query.EmpresasCadastradas();

                if (listEmpresa.Count == 0)
                {
                    return StatusCode(404, "Nenhuma empresa cadastrada");
                }

                return Ok(listEmpresa);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
                
            }
        }

        // GET api/values/fornecedores
        [HttpGet("fornecedores")]
        public ActionResult<List<Fornecedor>> GetFornecedores()
        {
            try
            {
                List<Fornecedor> listFornecedores = new List<Fornecedor>();
                listFornecedores = query.FornecedoresCadastrados();
                if (listFornecedores.Count == 0)
                {
                    return StatusCode(404, "Nenhum fornecedor cadastrado!");
                }

                return Ok(listFornecedores);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }

          
        }

        // GET api/values/listar
        [HttpGet("listar")]
        public ActionResult<List<Object>> GetListar([FromHeader] string filtro)
        {
            try
            {
                List<Object> listfiltro = new List<Object>();
                listfiltro = query.FiltraFornecedores(filtro);

                if (listfiltro.Count == 0 || String.IsNullOrEmpty(filtro))
                {
                    return StatusCode(404, "Não foi encontrado o fornecedor!");
                }

                return Ok(listfiltro);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }



        }

    }
}
