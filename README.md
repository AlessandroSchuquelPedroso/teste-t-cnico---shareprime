# Teste técnico - SharePrime

#### Tecnologias utilizadas:
| Tecnologias | 
|------|
|Visual Studio 2017 - WebApi .Net Core|
|Banco de dados(Local) - Sql Server|
|Postman|

# Metodos Disponiveis

#### POST -  <http://localhost:50097/api/values/cadastrar/empresa >
##### Cadastrar empresa
Cadastra as informações da empresa no banco de dados

##### Response Body Parameters 
Objeto montado no corpo Body(Json)
| Nome | Tipo| Descrição|
|------|-----|----------|
|Uf | string| sigla do estado |
|NomeFantasia | string| Nome fantasia da empresa |
|Cnpj|string|cnpj da empresa|

##### Exemplo:

```sh
{
	"Uf":"pr",
	"NomeFantasia":"Empresa teste2",
	"Cnpj": "04257076000199"
}
```
se tentar cadastrar um mesmo cnpj no banco de dados retornará um erro HTTP 403
##### Exemplo:
###### Status : 403 Forbidden

```sh
CNPJ já cadastrado!
```
#### POST -  <http://localhost:50097/api/values/cadastrar/fornecedor >
##### Cadastrar fornecedor
faz o cadastro do fornecedor no banco de dados

##### Response Body Parameters
Objeto no corpo Body
| Nome | Tipo| Descrição|
|------|-----|----------|
|Empresa | String| nome da empresa , caso seja pessoa jurídica|
|Nome| string| nome do cidadão|
|CpfCnpj| string| se for pessoa física informa o CPF, caso contrário CNPJ |
|Telefone| string|seu telefone |
|DataNascimento | string| caso seja pessoa fisica, informar data de nascimento |
|Rg|string|caso seja pessoa fisica, informar o RG|
##### Exemplo para pessoa física:
conforme a regra
```sh
{
	"Empresa": "",
	"Nome": "João da silva",
	"CpfCnpj":"03560684508",
	"Telefone":"51998152736",
	"DataNascimento":"16/11/2003",
	"Rg": "4095723865"
}
```
##### Exemplo para pessoa jurídica:
conforme a regra
```sh
{
	"Empresa": "empresa Dev",
	"Nome": "Alessandro Schuquel Pedroso",
	"CpfCnpj":"08297075000198",
	"Telefone":"5134437636",
	"DataNascimento":"",
	"Rg": ""
	
}
```
se tentar cadastrar um mesmo CNPJ ou CPF no banco de dados retornará um erro HTTP 403
##### Exemplo:
###### Status : 403 Forbidden

```sh
CPF OU CNPJ já cadastrado!
```

#### POST -  < http://localhost:50097/api/values/cadastrar/relacao >
##### Cadastra Empresa e fornecedor(relação)

faço uma relação entre fornecedor e empresa em uma tabela no banco de dados, informaçoes do seus ID serão passado no Header, conforme os parametros

##### Request Parameter

| Nome | Tipo| Descrição|
|------|-----|----------|
|idFornecedor | int| fornecedor pode ter relação com (N)Empresas| 
|idEmpresa| int | empresas pode ter relação com (N)Fornecedores|

##### Regra para empresa do estado do paraná caso seja pessoa física:
 sendo pessoa física identificado pelo CPF e a empresa do estado do paraná e menor de idade, o erro será mostrado na seguinte maneira:
 
 ##### Exemplo:
###### Status : 403 Forbidden

```sh
Empresa do Paraná não permite pessoa fisica menor de idade realizar o cadastro!
```



#### GET -  <http://localhost:50097/api/values/empresas >
##### Empresas Cadastradas

faz um select no banco retornando todas as empresas cadastradas.

##### Exemplo:
```sh
[
    {
        "idEmpresa": 1,
        "uf": "rs",
        "nomeFantasia": "Empresa teste",
        "cnpj": "08297075000198"
    },
    {
        "idEmpresa": 2,
        "uf": "pr",
        "nomeFantasia": "Empresa teste2",
        "cnpj": "04257076000199"
    }
]
```
 ##### Exemplo:
###### Status : 404 not found

```sh
Nenhuma empresa cadastrada
```
#### GET -  < http://localhost:50097/api/values/fornecedores> 
##### Fornecedores Cadastrados
Faz um select no banco buscando todos os fornecedores cadastrados.

##### Exemplo:
```sh
 [
    {
        "idFornecedor": 1,
        "empresa": "",
        "nome": "Alessandro Schuquel Pedroso",
        "cpfCnpj": "03560675056",
        "dataCadastro": "18/10/2019 02:49:33",
        "telefone": "51998152765",
        "dataNascimento": "16/11/1994",
        "rg": "4095723865"
    },
    {
        "idFornecedor": 2,
        "empresa": "empresa tecnologia",
        "nome": "Luciano da silva",
        "cpfCnpj": "08297075000198",
        "dataCadastro": "18/10/2019 02:51:51",
        "telefone": "51998152766",
        "dataNascimento": "",
        "rg": ""
    },
    {
        "idFornecedor": 3,
        "empresa": "",
        "nome": "Maria da silva",
        "cpfCnpj": "04560684508",
        "dataCadastro": "18/10/2019 02:53:40",
        "telefone": "51998152736",
        "dataNascimento": "16/11/1964",
        "rg": "4095723865"
    },
    {
        "idFornecedor": 4,
        "empresa": "",
        "nome": "João da silva",
        "cpfCnpj": "03560684508",
        "dataCadastro": "18/10/2019 02:57:35",
        "telefone": "51998152736",
        "dataNascimento": "16/11/2003",
        "rg": "4095723865"
    }
]
```
 ##### Exemplo:
###### Status : 404 not found

```sh
Nenhum fornecedor cadastrado!
```
## GET -  < http://localhost:50097/api/values/listar > 
##### Filtrar Fornecedores
realiza uma busca fazendo filtro informado no corpo Header por nome, CPF/CNPJ e também por data cadastrado no sistema.

##### Request Parameter

| Nome | Tipo| Descrição|
|------|-----|----------|
|filtro | string| recebe o que vem do filtro| 


##### Header Parameters

| Nome | Tipo| Descrição|
|------|-----|----------|
|filtro | string| faz o filtro por nome, CPF/CNPJ e data do cadastramento |

##### Exemplo:
```sh
[
    {
        "idEmpresa": 2,
        "idFornecedor": 1,
        "_clVirtualEmpresa": {
            "idEmpresa": 2,
            "uf": "pr",
            "nomeFantasia": "Empresa teste2",
            "cnpj": "04257076000199"
        },
        "_clVirtualFornecedor": {
            "idFornecedor": 1,
            "empresa": "",
            "nome": "Alessandro Schuquel Pedroso",
            "cpfCnpj": "03560675056",
            "dataCadastro": "18/10/2019 02:49:33",
            "telefone": "51998152765",
            "dataNascimento": "16/11/1994",
            "rg": "4095723865"
        }
    }
]

```

